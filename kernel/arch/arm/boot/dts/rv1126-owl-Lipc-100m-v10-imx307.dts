// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (c) 2020 Rockchip Electronics Co., Ltd.
 */

/dts-v1/;

#include "rv1126-owl-50ipc-v10-emmc.dtsi"

/ {
	model = "owlvisionTech LIPC 100M";
	compatible = "rockchip,rv1109-38-v10-spi-nand", "rockchip,rv1126";

	chosen {
		bootargs = "earlycon=uart8250,mmio32,0xff570000 console=ttyFIQ0 root=PARTUUID=614e0000-0000 rootfstype=ext4 rootwait snd_aloop.index=7";
	};

    vdd_lte_pwr: vdd-lte-pwr {
        compatible = "regulator-fixed";
        regulator-name = "vdd_lte_pwr";
        gpio = <&gpio2 RK_PA0 GPIO_ACTIVE_LOW>;
        enable-active-low;
        regulator-always-on;
        regulator-boot-on;
        regulator-min-microvolt = <3300000>;
        regulator-max-microvolt = <3300000>;
    };

    vdd_lte_en: vdd-lte-en {
        compatible = "regulator-fixed";
        regulator-name = "vdd_lte_en";
        gpio = <&gpio3 RK_PD4 GPIO_ACTIVE_HIGH>;
        enable-active-high;
        regulator-always-on;
        regulator-boot-on;
        pwm-supply = <&vdd_lte>;
        regulator-settling-time-up-us = <500>;
        regulator-min-microvolt = <4000000>;
        regulator-max-microvolt = <4000000>;
    };

    vdd_lte: vdd-lte {
        compatible = "regulator-fixed";
        regulator-name = "vdd_lte";
        gpio = <&gpio3 RK_PD7 GPIO_ACTIVE_LOW>;
        enable-active-low;
        regulator-always-on;
        regulator-boot-on;
        regulator-min-microvolt = <3300000>;
        regulator-max-microvolt = <3300000>;
    };

    vdd_lte_rst: vdd-lte-rst {
        compatible = "regulator-fixed";
        regulator-name = "vdd_lte_rst";
        gpio = <&gpio1 RK_PD6 GPIO_ACTIVE_HIGH>;
        enable-active-high;
        regulator-always-on;
        regulator-boot-on;
        regulator-min-microvolt = <4000000>;
        regulator-max-microvolt = <4000000>;
    };

    phy_rstn: phy-rstn {
        compatible = "regulator-fixed";
        regulator-name = "phy_rstn";
        gpio = <&gpio2 RK_PC0 GPIO_ACTIVE_LOW>;
        regulator-always-on;
        regulator-boot-on;
        regulator-min-microvolt = <3300000>;
        regulator-max-microvolt = <3300000>;
    };
};

&csi_dphy0 {
	status = "okay";

	ports {
		#address-cells = <1>;
		#size-cells = <0>;
		port@0 {
			reg = <0>;
			#address-cells = <1>;
			#size-cells = <0>;

			mipi_in_ucam2: endpoint@1 {
				reg = <1>;
				remote-endpoint = <&ucam_out2>;
				data-lanes = <4>;
				bus-type = <3>;
			};
		};
		port@1 {
			reg = <1>;
			#address-cells = <1>;
			#size-cells = <0>;

			csidphy0_out: endpoint@0 {
				reg = <0>;
				remote-endpoint = <&cif_mipi_in>;
				data-lanes = <4>;
				bus-type = <3>;
			};
		};
	};
};


&mipi_csi2 {
	status = "disabled";
};

&i2c1 {
	status = "okay";
	clock-frequency = <400000>;

	/delete-node/ imx415@1a ;

	imx307: imx307@1a {
		compatible = "sony,imx307";
		reg = <0x1a>;
        clocks = <&cru CLK_MIPICSI_OUT>;
        clock-names = "xvclk";
        power-domains = <&power RV1126_PD_VI>;
        pinctrl-names = "rockchip,camera_default";
        pinctrl-0 = <&mipicsi_clk0>;
		avdd-supply = <&vcc3v3_sys>;
		dovdd-supply = <&vcc_1v8>;
		dvdd-supply = <&vcc_dvdd>;
        pwdn-gpios = <&gpio3 RK_PC7 GPIO_ACTIVE_HIGH>;
        pd-gpios = <&gpio1 RK_PD4 GPIO_ACTIVE_HIGH>;
		reset-gpios = <&gpio1 RK_PD5 GPIO_ACTIVE_HIGH>;
		rockchip,camera-module-index = <1>;
		rockchip,camera-module-facing = "front";
		rockchip,camera-module-name = "MTV4-IR-E-P";
		rockchip,camera-module-lens-name = "40IRC-4MP-F16";
		ir-cut = <&cam_ircut0>;
		port {
			ucam_out2: endpoint {
				remote-endpoint = <&mipi_in_ucam2>;
				data-lanes = <4>;
				bus-type = <3>;
			};
		};
	};


};

&pmu_io_domains {
    status = "okay";
    vccio3-supply = <&vcc3v3_sys>;
};

&mdio {
    phy: phy@0 {
        compatible = "ethernet-phy-ieee802.3-c22";
        reg = <0x0>;
    };
};

&gmac {
    phy-mode = "rmii";
    clock_in_out = "output";

    snps,reset-gpio = <&gpio3 RK_PC5 GPIO_ACTIVE_LOW>;
    snps,reset-active-low;
    snps,reset-delays-us = <10000 100000 50000>;

    assigned-clocks = <&cru CLK_GMAC_SRC>, <&cru CLK_GMAC_TX_RX>;
    assigned-clock-parents = <&cru CLK_GMAC_SRC_M0>, <&cru RMII_MODE_CLK>;
    assigned-clock-rates = <50000000>;

    pinctrl-names = "default";
    pinctrl-0 = <&rmiim0_miim &rgmiim0_rxer &rmiim0_bus2 &rgmiim0_mclkinout_level0>;

    phy-handle = <&phy>;
    status = "okay";
};

&isp_reserved {
	size = <0x8000000>;
};

&rkcif_mipi_lvds {
	status = "okay";

	port {
		/* MIPI CSI-2 endpoint */
		cif_mipi_in: endpoint {
			remote-endpoint = <&csidphy0_out>;
			data-lanes = <4>;
			bus-type = <3>;
		};
	};
};

&uart5 {
    status = "okay";
    pinctrl-names = "default";
    dma-names = "tx", "rx";
    pinctrl-0 = <&uart5m1_xfer>;
};
